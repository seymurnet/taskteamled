FROM golang:alpine AS builder
RUN mkdir -p /go/bin
COPY ./hello.go /go/bin/
WORKDIR /go/bin/
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -tags netgo -ldflags '-w' hello.go 
FROM scratch
COPY --from=builder /go/bin/hello /go/bin/hello
EXPOSE 8080
ENTRYPOINT ["/go/bin/hello"]

